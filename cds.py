#Cheiro De Shangue App
from openerp import models, fields, api

#Extend product.template model with damage
class cds_product_template(models.Model):
        _name = 'product.template'
        _inherit = 'product.template'

        DMG = fields.Integer("Damage")
        Vida = fields.Integer("Vida")
        Armadura = fields.Integer("Armadura")
        stats_ids = fields.One2many('item.template.stat', 'item_id')


class cds_res_user_build(models.Model):
    _name = 'res.user.build'
    name = fields.Char('Build Name')
    build_date = fields.Datetime('Build Date')
    item_ids = fields.One2many('res.user.builditem','build_id')
    user_id = fields.Many2one('res.users','Build User')
    @api.one
    @api.depends('item_ids',"item_ids.quantity")
    def _calcdmg(self):
        currentdmg = 0
        for builditem in self.item_ids:
            currentdmg = currentdmg + builditem.item_id.DMG * builditem.quantity
        self.totaldmg = currentdmg
    totaldmg = fields.Integer(string='Total Damage', store=True, compute='_calcdmg')
    notes = fields.Text('Build Notes')
    build_op = fields.Boolean('Build OP')
    @api.onchange('totaldmg')
    def check_dmg(self):
        if self.totaldmg > 199:
            self.build_op = True
        else:
            self.build_op=False

class cds_res_user_builditem(models.Model):
    _name = 'res.user.builditem'
    build_id = fields.Many2one('res.user.build')
    item_id = fields.Many2one('product.template', 'Item')
    quantity = fields.Integer('Quantity')
    DMG = fields.Integer(related="item_id.DMG", string="Damage", store=True, readonly=True)
    notes = fields.Text("Build Item Notes")

class cdsItemStat(models.Model):
    _name = 'item.stat'
    name = fields.Char('Stat Name')
    uom_id = fields.Many2one('product.uom', string='Unit of Measure')
    description = fields.Text('Description')

class cdsItemTemplateStat(models.Model):
    _name = 'item.template.stat'
    stat_id = fields.Many2one('item.stat', string="Item Stat")
    item_id = fields.Many2one('product.template')
    uom = fields.Char(related='stat_id.uom_id.name', string='Unit Me', readonly=True)
    value = fields.Integer('Stat value')
