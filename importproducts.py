import xmlrpclib
import csv

server = 'http://localhost:8069'
database = 'cuzinho_de_frango'
user = 'admin'
pwd = 'admin'

common = xmlrpclib.ServerProxy('%s/xmlrpc/2/common'% server)

#print common.version()

uid = common.authenticate(database, user, pwd, {})

#print uid

OdooApi = xmlrpclib.ServerProxy('%s/xmlrpc/2/object'% server)

filter = [[('build_op','=','True')]]
product_count = OdooApi.execute_kw(database, uid, pwd, 'res.user.build', 'search_count', filter)

#print product_count

filename = "importdata.csv"

reader = csv.reader(open(filename, 'rb'))

filter = [[('name','=','Attack Damage')]]
categ_id = OdooApi.execute_kw(database, uid, pwd, 'product.category', 'search', filter)

for row in reader:

    productname = row[0]
    damage = row[1]
    filter = [[('name','=',productname)]]
    product_id = OdooApi.execute_kw(database, uid, pwd, 'product.template', 'search', filter)
    if product_id:

        record = [product_id, {'DMG':damage, 'categ_id':categ_id[0]}]
        product_id = OdooApi.execute_kw(database, uid, pwd, 'product.template', 'write', record)
        print 'the item "'+ productname +'" already exists in the database, updating existing data...'

    else:
        print 'adding "' + productname +'" to the database'
        record = [{'name': productname,'DMG':calories, 'categ_id':categ_id[0]}]
        OdooApi.execute_kw(database, uid, pwd, 'product.template', 'create', record)
